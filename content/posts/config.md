+++
title="Config ?KoRo"
date=2022-05-19
draft=false

[taxonomies]

tags=["configs", "settings"]

[extra]

disable_comments = true
+++

### Startup parameters:

``-novid +fps_max 0 +mat_queue_mode 2 +rate 786432 +cl_interp 1 +cl_interp_ratio 1 -high -tickrate 128``

### Crosshairs:

``CSGO-BHOxG-ZJxtC-dyvBX-b6QUG-KQxoO``
``CSGO-VcKKB-8SDSk-DfR8s-iMYtc-TUzfF``

### Config:

[?KoRo.cfg][1]

[1]: https://settings.gg/player/1030287014
