+++
title="Сonfig Moonfar."
date=2022-05-19
draft=false

[taxonomies]

tags=["configs", "settings"]

[extra]

disable_comments = true
+++

### Startup parameters:

``-novid -freq 120 -tickrate 128 -high -nojoy``

### Crosshair:

``CSGO-ZQJK3-5y2xU-DMd2X-E4Opc-h6LsE``

### Config:

[ihatemoonfar.cfg][1]

[1]: https://settings.gg/player/328279436