+++
title="Enragedpeek"
date=2022-05-20
draft=false

[taxonomies]

tags=["info", "links"]

[extra]

disable_comments = true
+++

## Information about the EnragedPeek team
The team was created in 2022, but its original name was Toyotapeek. Yes, yes, but because the team went to the RedBull Flick Tournament closed qualifiers it had to be renamed, due to advertising. The founder of the team is a player under the nickname Moonfar or Farukh. At the moment the team has 2 players, because the team was created for the tournament 2x2.

## Team composition:
### [Moonfar.][6] - [Faceit][8]
### [?KoRo][5] - [Faceit][7]
## [Pinger][1] [Faceit][2] [Telegram][3] [Steam][4]
[1]: https://pinger.kz/teams/5d944f19-8293-4105-9b48-4052dfcbe056/
[2]: https://www.faceit.com/en/teams/2170bc86-5727-4327-b410-58991763c0c1/
[3]: https://t.me/enragedpeek/
[4]: https://steamcommunity.com/groups/enragedpeek/
[5]: https://steamcommunity.com/id/jj0e3q/
[6]: https://steamcommunity.com/id/moonfarr/
[7]: https://www.faceit.com/en/players/KoRoPPG
[8]: https://www.faceit.com/en/players/ChameleonMLG